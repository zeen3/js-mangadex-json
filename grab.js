// group grabbing script
// run from https://mangadex.org/upload/47
{
    let dat = Array.prototype.slice.call(group_id.options, 1);
    let te = new TextEncoder;
    let a = [];
    let u31 = v => {
        while(v > 0x7F) {
            a.push(0x80 | (v & 0x7F));
            v >>>= 7
        }
        a.push(v)
    };
    let grp, s;
    while (grp = dat.pop()) {
        u31(grp.value >>> 0);
        s = te.encode(grp.innerText);
        u31(s.length);
        Array.prototype.push.apply(a, s);
    }
    Object.assign(document.createElement('a'), {
        "href": URL.createObjectURL(new Blob([new Uint8Array(a)])),
        "download":"groups.dat"
    }).click()
}
