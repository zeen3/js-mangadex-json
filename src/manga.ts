import { Language, noemp, Languages, Named, check_group_cache, mark, wrap_measure, measure, gc } from "./common";

export interface MangaChapterAPI {
    id: number;
    timestamp: Date;
    volume: number | null;
    chapter: noemp;
    ch_num: number;
    title: noemp;
    lang: Language;
    groups: [Named, Named | null, Named | null];
}

export interface MangaLinksAPI {
  /** Bookwalker ID */
  bw: noemp;
  /** MangaUpdates ID */
  mu: number;
  /** NovelUpdates slug */
  nu: noemp;
  /** Amazon.co.jp URL */
  amz: noemp;
  /** CDJapan URL */
  cdj: noemp;
  /** eBookJapan URL */
  ebj: noemp;
  /** MyAnimeList ID */
  mal: number;
  /** Raw URL */
  raw: noemp;
  /** Official Eng URL */
  engtl: noemp;
}

export interface MangaAPI {
    title: noemp;
    desc: noemp;
    artist: noemp;
    author: noemp;
    status: number;
    genres: boolean[];
    last_chapter: noemp;
    lang: Language;
    hentai: boolean;
    links: MangaLinksAPI;
    cover: string;
    chapters: null | MangaChapterAPI[];
}
const genres = (g?: number[]): boolean[] => {
    let gr = Array(96)["fill"](false);
    if (g) for (let i = g.length; i--;) gr[g[i]] = true;
    return gr;
}

const inf_or_val = (val: null | undefined | number): number => val == null ? Infinity : val
const sort_chs = (a: MangaChapterAPI, b: MangaChapterAPI) => (
    // if both have volumes, bubble null to the top, keep everything else as top level
    (a.volume != b.volume && inf_or_val(a.volume) - inf_or_val(b.volume)) ||
    // if both have chapters, bubble the higher chapter value to the top
    (a.chapter && b.chapter && a.chapter != b.chapter && a.ch_num - b.ch_num) ||
    // yeah sorting by language can cause issues.
    (a.lang - b.lang) ||
    // does not handle unicode differences well; presumes a lot more than you'd want.
    (a.title && b.title && (a.title != b.title && a.title > b.title ? 1 : -1) ) ||
    // obviously since `null` is 0 it gets sorted to the top but... in our case it's minimal.
    gc["indexOf"](a.groups[0]) - gc["indexOf"](b.groups[0]) ||
    gc["indexOf"](a.groups[1]) - gc["indexOf"](b.groups[1]) ||
    gc["indexOf"](a.groups[2]) - gc["indexOf"](b.groups[2]) ||
    // if all of our groups are 0 (or whatever); we sort by timestamp because we really have a weird chapter that's probably a duplicate
    +a.timestamp - +b.timestamp ||
    // if our IDs are the same we've really messed up
    a.id - b.id
);
export const decode_manga = wrap_measure('decode-manga', (json: any): MangaAPI | null => {
    if (json["status"] != "OK") return null;
    let chapters: MangaChapterAPI[] = [], c = json["manga"], l = c["links"]||{}, v, id;
    let links: MangaLinksAPI = {
        mu: parseInt(l["mu"], 10) >>> 0,
        mal: parseInt(l["mal"], 10) >>> 0,
        nu: noemp(l["nu"]),
        raw: noemp(l["raw"]),
        engtl: noemp(l["engtl"]),
        cdj: noemp(l["cdj"]),
        amz: noemp(l["amz"]),
        ebj: noemp(l["ebj"]),
        bw: noemp(l["bw"])
    };
    let mapi: MangaAPI = {
        status: c["status"] & 7,
        title: noemp(c["title"]),
        desc: noemp(c["description"]),
        artist: noemp(c["artist"]),
        author: noemp(c["author"]),
        genres: genres(c["genres"]),
        last_chapter: (v = c["last_chapter"]) == "0" ? null : v,
        lang: Language[c["lang_flag"] as Languages],
        hentai: c["hentai"] == 1,
        links,
        cover: c["cover_url"],
        chapters,
    };

    mark('ch-decode');
    if ((l=json["chapter"]) != null) for (id in l) (c = l[id], chapters["push"]({
        id: parseInt(id, 10),
        timestamp: new Date(c["timestamp"] as number * 1_000),
        volume: (v = parseInt(c["volume"], 10), isNaN(v) ? null : v>>>0),
        chapter: noemp(c["chapter"]),
        ch_num: parseFloat(c["chapter"]),
        title: noemp(c["title"]),
        lang: Language[c["lang_code"] as Languages],
        groups: [
            check_group_cache(c["group_id"], c["group_name"])!,
            check_group_cache(c["group_id_2"], c["group_name_2"]),
            check_group_cache(c["group_id_3"], c["group_name_3"])
        ]
    }));
    measure('ch-decode');
    mark('ch-sort');
    chapters["sort"](sort_chs);
    measure('ch-sort');

    return mapi;
})