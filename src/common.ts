import { MangaChapterAPI, MangaAPI } from "./manga";

export enum Language { unknown, gb, jp, pl, rs, nl, it, ru, de, hu, fr, fi, vn, gr, bg, es, br, pt, se, sa, dk, cn, bd, ro, cz, mn, tr, id, kr, mx, ir, my, th, ct, ph, hk, ua, mm, lt }
export enum LanguageCode { other, en, ja, pl, sh, nl, it, ru, de, hu, fr, fi, vi, el, bg, es, ptBR, pt, sv, ar, da, zhHans, bn, ro, cs, mn, tr, id, ko, es419, fa, ms, th, ca, fil, zhHant, uk, my, lt }
export type Languages = keyof typeof Language;
export type noemp = Exclude<string, ""> | null;
export const noemp: ((s: any) => noemp) = s => s && ""+s || null;
//@ts-ignore
export const fbind = <F extends (this: T, args: any[]) => R, T, R>(fn: F): ((self: T, ...args: Parameters<F>) => R) => Function["prototype"]["call"]["bind"](fn)

export interface Named {
    /** unsigned integer */
    id: number;
    /** string (always) */
    name: string;
}
function* filter_map<T, U>(list: T[] | IterableIterator<T>, f: ((t: T) => U | null | undefined)): IterableIterator<U> {
    let v, k
    for (k of list) if ((v = f(k)) != null) yield v;
}
export type NamedList = (Named | null)[];

/** Cache of group names. */
export const gc: NamedList = [null, {id: 1, name: "Doki Fansubs"}];
gc.length = 10_000;
export const check_group_cache = (id: number, name: string): Named | null => (
    id in gc ? gc[id] : (gc[id] = { id: id>>>0, name: ""+name })
);

/// performance measures
export const mark = performance["mark"]["bind"](performance);
const _measure = performance["measure"]["bind"](performance)
export const measure = (name: string) => _measure(name, name)
export const measure_fn = <U extends (...args: any[]) => V, V>(name: string, fn: U, ...args: Parameters<U>): V => {
    try {
        mark(name);
        return fn(...args);
    } finally {
        measure(name);
    }
}
//@ts-ignore
export const wrap_measure = <U extends (...args: any[]) => V, V>(name: string, fn: U): U => (...args: Parameters<U>): V => measure_fn(name, fn, ...args);

export const measure_fn_async = async <U, V>(name: string, fn: (arg: U) => Promise<V>, arg: U) => {
    try {
        mark(name);
        return await fn(arg);
    } finally {
        measure(name);
    }
}
//@ts-ignore
export const wrap_measure_async = <U extends (arg: T) => Promise<V>, T, V>(name: string, fn: U): U => arg => measure_fn_async(name, fn, arg);

export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;

// dom stuff

/** yeah this is odd without tsignore */
//@ts-ignore
export const decode: ((img: HTMLImageElement) => Promise<void>) = wrap_measure_async<(arg: undefined) => void, never, void>('img-decode', HTMLImageElement["prototype"]["decode"]
? fbind(HTMLImageElement["prototype"]["decode"]) as ((img: HTMLImageElement) => Promise<void>)
: (img: HTMLImageElement) => new Promise<void>((r: () => void, j) => {
    if (img["complete"]) return r();
    const res = (): void => (
        img["complete"] ? r() : requestAnimationFrame(() => r()),
        img["removeEventListener"]("error", rej),
        img["removeEventListener"]("load", res)
    );
    const rej: ((e: ErrorEvent) => void) = e => (
        j(e),
        img["removeEventListener"]("error", rej),
        img["removeEventListener"]("load", res)
    );
    img["addEventListener"]("error", rej, {"once": true});
    img["addEventListener"]("load", res, {"once": true});
}));

type Queryable = Document | DocumentFragment | Element;
interface closest {
    <T extends keyof HTMLElementTagNameMap, K extends Element>(
        sels: T,
        ctx: K
    ): HTMLElementTagNameMap[T];
    <T extends keyof SVGElementTagNameMap, K extends Element>(
        sels: T,
        ctx: K
    ): SVGElementTagNameMap[T];
    <T extends Element>(
        sels: string,
        ctx: Element,
        tp: new() => T
    ): T;
}
type Appendable = HTMLElement | string | DocumentFragment;
interface queryselector {
    <T extends keyof HTMLElementTagNameMap>(
        sels: T,
        ctx?: Queryable
    ): HTMLElementTagNameMap[T];
    <T extends keyof SVGElementTagNameMap>(
        sels: T,
        ctx?: Queryable
    ): SVGElementTagNameMap[T];
    <T extends Element>(
        sels: string,
        ctx: Queryable,
        tp: new() => T
    ): T;
}
interface queryselectorall {
    <T extends keyof HTMLElementTagNameMap>(
        sels: T,
        ctx?: Queryable
    ): HTMLElementTagNameMap[T][];
    <T extends keyof SVGElementTagNameMap>(
        sels: T,
        ctx?: Queryable
    ): SVGElementTagNameMap[T][];
    <T extends Element>(
        sels: string,
        ctx: Queryable,
        tp: new() => T
    ): T[];
}
interface createelement<E> {
    <T extends keyof E>(el: T, o?: Partial<Omit<E[T], "children" | "childNodes">>): E[T];
}

export const $f = () => document["createDocumentFragment"]();
export const closest: closest = <T>(sels: string, ctx: Element, tp?: new() => T): T => {
    let cls = tp || HTMLElement,
    el = ctx["closest"](sels);
    if (el instanceof cls) return el as T;
    throw el ? 1 : -1;
};
export const $: queryselector = <T>(sels: string, ctx: Queryable = document, tp?: new() => T): T => {
    let cls = tp || HTMLElement,
    el = ctx["querySelector"](sels);
    if (el instanceof cls) return el as T;
    throw el ? 1 : -1;
};
export const $$: queryselectorall = <T>(sels: string, ctx: Queryable = document, tp?: new() => T): T[] => {
    let cls = tp || HTMLElement,
    els = ctx["querySelectorAll"](sels),
    i = 0,
    j = 0,
    l = els.length,
    rt: T[] = Array(l);
    do els[i] instanceof cls && (rt[j++] = els[i] as unknown as T);
    while (++i < l)
    rt.length = j;
    return rt;
};
export const $ce: createelement<HTMLElementTagNameMap> = (el, o) => Object["assign"](document["createElement"](el), o||{})
export type MaybeOr<T> = T | false | 0 | null | '' | undefined;
export type IterableOrNot<T> = MaybeOr<Iterable<MaybeOr<T>>>;
//export const $ces: createelement<SVGElementTagNameMap> = (el, o) => Object["assign"](document["createElementNS"]("http://www.w3.org/2000/svg", el), o)
export const $cec = <T extends keyof HTMLElementTagNameMap>(
    name: T,
    children?: IterableOrNot<Appendable>,
    attrs?: Omit<Partial<HTMLElementTagNameMap[T]>, "children" | "childNodes" | "innerHTML" | "innerText" | "outerHTML">,
) => {
    let container = $ce(name, attrs as any), el: MaybeOr<Appendable>;
    if (children) for (el of children) el && container["append"](el);
    return container
};
export const $cc = (n: string, c?: IterableOrNot<Appendable>) => $cec("div", c, {"className": n});
export const $cs = (n: string, c?: IterableOrNot<Appendable>) => $cec("span", c, {"className": n});


//const timestamp_to_date = (t: {timestamp: number;}) => new Date(t.timestamp);
const ch_to_el = (c: MangaChapterAPI) => $cc('chp', [
    $cec("a", [
        $cs("chv", [c.volume != null && String(c.volume)]),
        $cs("chn", [c.chapter]),
        $cs("cht", [c.title])
    ], {
        "href": "https://mangadex.org/chapter/" + c.id,
        "className": "chapinfo",
        "hreflang": LanguageCode[c.lang]
    }),
    $cc("flag flag-" + c.lang, [LanguageCode[c.lang]]),
    $cc('groups', filter_map(c.groups, n => n && $ce("a", {
        "className": "grp",
        "href": 'https://mangadex.org/group/' + n.id,
        "innerText": n.name
    }))),
    $ce("time", {
        "innerText": c.timestamp["toUTCString"](),
        "dateTime": c.timestamp["toISOString"]()["slice"](0,-5)+"Z",
        get "title"(){return this["dateTime"]["replace"]('T',' ')}
    })
]);
let dl: boolean[] = Array(64)["fill"](true);
const lang_filter = (langs?: Language[]): boolean[] => {
    if (!langs || !langs.length) return dl;
    let l = Array(64)["fill"](false), i = langs.length;
    while (i--) l[langs[i]] = true;
    return l;
}

type Holes = [number, number][];
/**
 * Find holes in chapter lists
 * This is actually a lot better than the one holo uses, and is O(n + log2(n)) time.
 */
export const holes =  wrap_measure('holes', ({chapters}: MangaAPI, langs?: Language[]) => {
    if (chapters) {
        // prefix langs
        let filter = lang_filter(langs)
        // predefine 0 as not many have a 0.
        let a = [0>>>0], i = chapters.length, c, v, holes_found: Holes = [], prev = 0, next
        // round values down
        while (c = chapters[--i]) if (filter[c.lang]) a[v = (c.ch_num >>> 0)] = v;
        // O(n) rather than O(max(n)) (key based iteration)
        for (next in a) {
            v = a[next];
            if (v > prev++)
                holes_found["push"]([prev-1, (prev=v+1)-2]);
        }
        return holes_found
    }
    throw 1
});

export const $holes = wrap_measure('$holes', (holes_found: Holes) => holes_found.length
    ? $cec("ul", filter_map(
        holes_found,
        f => $ce("li", {"innerText": f[0] == f[1] ? ""+f[0] : `${f[0]}..=${f[1]}`})
    ))
    : null
);
export const $manga_ch_list = wrap_measure('manga ch list', ({chapters}: MangaAPI, langs?: Language[]) => {
    if (!chapters) return $cc("mangas", []);
    // prefix langs
    let filter = lang_filter(langs)
    return $cc("manga", filter_map(chapters, ch => filter[ch.lang] ? ch_to_el(ch) : null));
});
const $ae = (id: string, tx: string, v: number | noemp, b?: string) => v ? $ce("a", {
    "target": "_blank",
    "rel": "noopener noreferer",
    "className": "ext portal",
    "id": "ex-" + id,
    "href": (b || "") + v,
    "innerText": tx
}) : null;

export const $links = wrap_measure('$links', ({links: l}: MangaAPI) => $cc("links", [
    $ae("raw", "Raw", l.raw),
    $ae("engtl", "Official English", l.engtl),
    $ae("amz", "Amazon.co.jp", l.amz),
    $ae("bw", "Book\xadwalker", l.bw && l.bw + '/', "https://bookwalker.jp/"),
    $ae("cdj", "CD\xadJapan", l.cdj),
    $ae("ebj", "eBook\xadJapan", l.ebj),
    $ae("mu", "Manga\xadUpdates", l.mu, "https://www.mangaupdates.com/series.html?id="),
    $ae("mal", "My\xadAnime\xadList", l.mal, "https://myanimelist.net/manga/"),
    $ae("nu", "Novel\xadUpdates", l.nu && l.nu + '/', "https://www.novelupdates.com/series/")
]));