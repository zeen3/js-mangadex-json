import { Language, Languages, noemp, gc, Named, $ce, decode, wrap_measure } from "./common";

//export interface Pages {
//    server: string;
//    data: string;
//    pages: string[];
//    max: number;
//}
type Pages = string[];
export interface ChapterAPI {
    id: number;
    timestamp: Date;
    volume: number | null;
    chapter: noemp;
    title: noemp;
    lang: Language;
    manga_id: number;
    //group_ids: [number, number, number];
    groups: [Named, Named | null, Named | null];
    comments: number | null;
    pages: null | Pages;
    long_strip: boolean;
}
const to_pages = (server: noemp, hash: noemp, page_array: string[]|null): Pages | null => {
    if (!(server && hash && page_array)) return null;
    let srv = server == "/data/" ? "https://mangadex.org"+server : server;
    let i = page_array.length;
    let pages: string[] = Array(i);
    for (;i--;) pages[i] = "" + srv + hash + "/" + page_array[i];
    return pages;
}
/** a stub for now. */
export const get_image_await_decode = (ch: ChapterAPI, n: number, dist: number = 0) => {
    if (ch.pages) {
        if (n < ch.pages.length && n > 0) {
            const img = $ce("img", {
                "src": ch.pages[n - 1],
                "importance": dist < 2 && dist > -1 ? "high" : "low",
                "decoding": "async",
            } as any); // fuck it doesn't move fast enough
            return { el: img, decoded: decode(img) };
        }
        // something something portal
    }
    return null
};
let v: number = 0|0;
export const decode_chapter = wrap_measure('decode-chapter', (json: any): ChapterAPI | null => json["status"] != "OK" ? null : {
    id: json["id"] >>> 0,
    long_strip: json["long_strip"] == 1,
    timestamp: new Date(json["timestamp"] as number * 1_000),
    lang: Language[json["lang_code"] as Languages],
    manga_id: json["manga_id"] >>> 0,
    comments: json["comments"] >>> 0,
    groups: [
        gc[json["group_id"]]!,
        gc[json["group_id_2"]] || null,
        gc[json["group_id_3"]] || null,
    ],
    //group_ids: [
    //    j["group_id"] >>> 0,
    //    j["group_id_2"] >>> 0,
    //    j["group_id_3"] >>> 0
    //],
    title: noemp(json["title"]),
    volume: (v = parseInt(json["volume"], 10), isNaN(v) ? null : v>>>0),
    chapter: noemp(json["chapter"]),
    pages: to_pages(json["server"], json["hash"], json["page_array"]),
});