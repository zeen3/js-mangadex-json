const common = import("./common");
export let gfn = async (res: ArrayBuffer) => {
    let i = 0 >>> 0, v = 0|0, curr = 0|0;
    const u31 = () => {
        v = data[i++], curr = v & 0x7F;
        if (v & 0x80) {
            curr |= ((v = data[i++]) & 0x7F) << 7;
            if (v & 0x80) {
                curr |= ((v = data[i++]) & 0x7F) << 14;
                if (v & 0x80) {
                    curr |= ((v = data[i++]) & 0x7F) << 21;
                    if (v & 0x80) {
                        curr |= ((v = data[i++]) & 0x07) << 28;
                    };
                };
            };
        };
    };
    const data = new Uint8Array(res);
    const str = new TextDecoder('utf-8', {
        "fatal": true,
        "ignoreBOM": true
    });
    const len = data.length;
    const { "gc": gc } = await common;
    while (i < len) {
        gc[u31(), curr] = {
            id: curr,
            name: str['decode'](data['subarray']((u31(), i), i += curr))
        };
    };
};
export const groups = fetch('groups.dat')
    .then(res => res['arrayBuffer']())
    .then(gfn);
